### SUMMARY
This module provides an entity config for dynamic library declarations.

### INSTALLATION
Using composer:
```
composer require drupal/entity_library --sort-packages
```

### SPONSORS
- [Fundación UNICEF Comité Español](https://www.unicef.es)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](https://www.drupal.org/u/pcambra)
- Manuel Egío [(facine)](https://www.drupal.org/u/facine)
