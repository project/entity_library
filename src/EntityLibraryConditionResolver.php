<?php

namespace Drupal\entity_library;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\entity_library\Entity\EntityLibraryInterface;

/**
 * Defines a class to resolver entity library conditions.
 */
class EntityLibraryConditionResolver {

  use ConditionAccessResolverTrait;

  /**
   * The plugin context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * The context manager service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * Creates an EntityLibraryConditionResolver object.
   *
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   *   The ContextHandler for applying contexts to conditions properly.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The lazy context repository service.
   */
  public function __construct(ContextHandlerInterface $context_handler, ContextRepositoryInterface $context_repository) {
    $this->contextHandler = $context_handler;
    $this->contextRepository = $context_repository;
  }

  /**
   * Evaluates the given conditions.
   *
   * @param \Drupal\entity_library\Entity\EntityLibraryInterface $entity_library
   *   The entity library which contain the conditions to be evaluated.
   *
   * @return bool
   *   TRUE if access is granted or FALSE if access is denied.
   */
  public function evaluateConditions(EntityLibraryInterface $entity_library) {
    $conditions = [];
    $missing_context = FALSE;
    foreach ($entity_library->getConditions() as $condition_id => $condition) {
      if ($condition instanceof ContextAwarePluginInterface) {
        try {
          $contexts = $this->contextRepository->getRuntimeContexts(array_values($condition->getContextMapping()));
          $this->contextHandler->applyContextMapping($condition, $contexts);
        }
        catch (ContextException $exception) {
          $missing_context = TRUE;
        }
      }
      $conditions[$condition_id] = $condition;
    }

    if (!$missing_context && $this->resolveConditions($conditions, $entity_library->getLogic()) !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }

}
