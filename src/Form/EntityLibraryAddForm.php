<?php

namespace Drupal\entity_library\Form;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides add form for entity libraries instance forms.
 */
class EntityLibraryAddForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#title'] = $this->t('Add an entity library');
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('The human-readable name of this entity library. This name must be unique.'),
      '#required' => TRUE,
      '#size' => 30,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\entity_library\Entity\EntityLibrary', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this entity library. It must only contain lowercase letters, numbers, and underscores.'),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $context = [
      '%name' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'), 'collection')->toString(),
    ];
    $this->logger('entity_library')->notice('Added entity library %name.', $context);

    $this->messenger()->addStatus($this->t('The entity library has been added.'));
    $form_state->setRedirectUrl($this->entity->toUrl('edit-form'));
  }

}
