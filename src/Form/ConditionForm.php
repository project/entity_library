<?php

namespace Drupal\entity_library\Form;

use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\entity_library\Ajax\CloseConditionDialogCommand;
use Drupal\entity_library\Entity\EntityLibraryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides add form for condition instance forms.
 */
class ConditionForm extends FormBase {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The entity library.
   *
   * @var \Drupal\entity_library\Entity\EntityLibraryInterface
   */
  protected $entityLibrary;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a ConditionForm object.
   *
   * @param \Drupal\Core\Condition\ConditionManager $condition_manager
   *   The condition plugin manager.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The lazy context repository service.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   */
  public function __construct(ConditionManager $condition_manager, ContextRepositoryInterface $context_repository, EntityFormBuilderInterface $entity_form_builder, Request $request = NULL) {
    $this->conditionManager = $condition_manager;
    $this->contextRepository = $context_repository;
    $this->formBuilder = $entity_form_builder;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('entity.form_builder'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_library_condition_configure';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityLibraryInterface $entity_library = NULL, $id = NULL) {
    $this->entityLibrary = $entity_library;

    $form_state->setTemporaryValue('gathered_contexts', $this->contextRepository->getAvailableContexts());

    if (Uuid::isValid($id)) {
      $instance = $this->entityLibrary->getCondition($id);
    }
    else {
      $instance = $this->conditionManager->createInstance(str_replace('-', ":", $id), []);
    }

    /** @var \Drupal\Core\Condition\ConditionInterface $instance */
    $form = $instance->buildConfigurationForm($form, $form_state);

    if (Uuid::isValid($id)) {
      $form['id'] = [
        '#type' => 'value',
        '#value' => $id,
      ];
    }
    $form['instance'] = [
      '#type' => 'value',
      '#value' => $instance,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#ajax' => [
        'callback' => [$this, 'closeForm'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Condition\ConditionInterface $instance */
    $instance = $form_state->getValue('instance');
    $instance->submitConfigurationForm($form, $form_state);

    if ($instance instanceof ContextAwarePluginInterface) {
      /** @var \Drupal\Core\Plugin\ContextAwarePluginInterface $instance */
      $context_mapping = $form_state->hasValue('context_mapping') ? $form_state->getValue('context_mapping') : [];
      $instance->setContextMapping($context_mapping);
    }

    if ($form_state->hasValue('id')) {
      $conditions = $this->entityLibrary->getConditions()->getConfiguration();
      $conditions[$form_state->getValue('id')] = $instance->getConfiguration();
      $this->entityLibrary->set('entity_library', $conditions);
    }
    else {
      $this->messenger()->addStatus($this->t('The condition has been added.'));
      $this->entityLibrary->addCondition($instance->getConfiguration());
    }
    $this->entityLibrary->save();

    if (!$this->request->isXmlHttpRequest()) {
      $form_state->setRedirectUrl($this->entityLibrary->toUrl('edit-form'));
    }
  }

  /**
   * Callback for closing the modal condition form.
   */
  public function closeForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $entityLibraryForm = $this->formBuilder->getForm($this->entityLibrary, 'edit');
    $response->addCommand(new ReplaceCommand('#entity-library-conditions', $entityLibraryForm['items']));
    // @todo Try to close the off canvas dialog.
    $response->addCommand(new RedirectCommand($this->entityLibrary->toUrl('edit-form')->toString()));

    return $response;
  }

}
