<?php

namespace Drupal\entity_library\Form;

use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\ConfirmFormHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_library\Entity\EntityLibraryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides delete form for condition instance forms.
 */
class ConditionDeleteForm extends ConfirmFormBase {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The entity library.
   *
   * @var \Drupal\entity_library\Entity\EntityLibraryInterface
   */
  protected $entityLibrary;

  /**
   * Constructs a ConditionForm object.
   *
   * @param \Drupal\Core\Condition\ConditionManager $manager
   *   The condition plugin manager.
   */
  public function __construct(ConditionManager $manager) {
    $this->conditionManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.condition')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_library_condition_configure';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion($id = NULL) {
    $condition = $this->entityLibrary->getCondition($id);

    return $this->t('Are you sure you want to delete the @label condition?', [
      '@label' => $condition->getPluginId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.entity_library.edit_form', ['entity_library' => $this->entityLibrary->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntityLibraryInterface $entity_library = NULL, $id = NULL) {
    $this->entityLibrary = $entity_library;

    $form['#title'] = $this->getQuestion($id);;
    $form['#theme'] = 'confirm_form';
    $form['#attributes']['class'][] = 'confirmation';

    $form['description'] = [
      '#markup' => $this->t('This action cannot be undone.'),
    ];
    $form['confirm'] = [
      '#type' => 'hidden',
      '#value' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'value',
      '#value' => $id,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#submit' => [
        [$this, 'submitForm'],
      ],
    ];
    $form['actions']['cancel'] = ConfirmFormHelper::buildCancelLink($this, $this->getRequest());

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $conditions = $this->entityLibrary->getConditions()->getConfiguration();

    $id = $form_state->getValue('id');
    if (array_key_exists($id, $conditions)) {
      unset($conditions[$id]);
      $this->entityLibrary->set('conditions', $conditions);
      $this->entityLibrary->save();
      $this->messenger()->addStatus($this->t('The condition has been deleted.'));
    }

    $form_state->setRedirect('entity.entity_library.edit_form', ['entity_library' => $this->entityLibrary->id()]);
  }

}
