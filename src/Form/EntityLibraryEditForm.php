<?php

namespace Drupal\entity_library\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenOffCanvasDialogCommand;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for entity library add/edit forms.
 */
class EntityLibraryEditForm extends EntityForm {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The context repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a EntityLibraryForm object.
   *
   * @param \Drupal\Core\Condition\ConditionManager $condition_manager
   *   The ConditionManager for building the conditions UI.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The lazy context repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language
   *   The language manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   */
  public function __construct(ConditionManager $condition_manager, ContextRepositoryInterface $context_repository, LanguageManagerInterface $language, FormBuilderInterface $form_builder) {
    $this->conditionManager = $condition_manager;
    $this->contextRepository = $context_repository;
    $this->formBuilder = $form_builder;
    $this->languageManager = $language;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.condition'),
      $container->get('context.repository'),
      $container->get('language_manager'),
      $container->get('form_builder')
    );
  }

  /**
   * Callback for opening the modal add condition form.
   */
  public function addForm(array &$form, FormStateInterface $form_state) {
    $condition = $form_state->getValue('add_conditon');
    $content = $this->formBuilder->getForm(ConditionForm::class, $this->entity, $condition);

    $route_parameters = [
      'entity_library' => $this->entity->id(),
      'id' => $condition,
    ];
    $options = ['query' => [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE]];
    $content['#attached']['library'][] = 'core/drupal.dialog.off_canvas';
    $content['submit']['#attached']['drupalSettings']['ajax'][$content['submit']['#id']]['url'] = Url::fromRoute('entity.entity_library.condition', $route_parameters, $options)->toString();

    $response = new AjaxResponse();
    $response->addCommand(new OpenOffCanvasDialogCommand(
        $this->t('Configure @label context', ['@label' => mb_strtolower($this->getContextOptionsForm()[$condition])]),
        $content,
        ['width' => '700'])
    );

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\entity_library\Entity\EntityLibraryInterface $entity */
    $entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('The human-readable name of this entity library. This name must be unique.'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => EntityTypeInterface::ID_MAX_LENGTH,
      '#description' => $this->t('A unique name for this entity library instance. Must be alpha-numeric and underscore separated.'),
      '#default_value' => !$entity->isNew() ? $entity->id() : NULL,
      '#machine_name' => [
        'exists' => '\Drupal\entity_library\Entity\EntityLibrary::load',
        'replace_pattern' => '[^a-z0-9_.]+',
        'source' => ['label'],
      ],
      '#required' => TRUE,
      '#disabled' => !$entity->isNew(),
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $entity->getDescription(),
    ];

    // @TODO Add library files validation.
    $form['library_info'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Library info'),
      '#description' => $this->t('Same structure as <a href="@defining-libraries">*.libraries.yml</a> but without the library name, it is the ID of this entity. You must use absolute paths.', ['@defining-libraries' => 'https://www.drupal.org/docs/8/creating-custom-modules/adding-stylesheets-css-and-javascript-js-to-a-drupal-8-module#define-library']),
      '#default_value' => $entity->getLibraryInfo(),
      '#attributes' => ['data-yaml-editor' => 'true'],
    ];
    if (!\Drupal::moduleHandler()->moduleExists('yaml_editor')) {
      $t_args = ['@yaml-editor' => 'https://www.drupal.org/project/yaml_editor'];
      $this->messenger()->addWarning($this->t('It is recommended to install the <a href="@yaml-editor">YAML Editor</a> module for easier editing.', $t_args));
      $form['library_info']['#rows'] = count(explode("\n", $form['library_info']['#default_value'])) + 3;
      $form['library_info']['#default_value'] = $entity->getLibraryInfo();
    }
    else {
      $form['library_info']['#default_value'] = Yaml::encode($entity->getLibraryInfo());
    }

    $form['entity_library_conditions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];
    $form['entity_library_conditions']['logic'] = [
      '#type' => 'select',
      '#options' => [
        'and' => $this->t('@logic (For all conditions)', ['@logic' => $this->t('AND')]),
        'or' => $this->t('@logic (For all conditions)', ['@logic' => $this->t('OR')]),
      ],
      '#default_value' => $entity->getLogic(),
      '#parents' => ['logic'],
    ];
    $form['entity_library_conditions']['add_conditon'] = [
      '#type' => 'select',
      '#options' => $this->getContextOptionsForm(),
    ];
    $form['entity_library_conditions']['add_button'] = [
      '#type' => 'submit',
      '#name' => 'add',
      '#value' => $this->t('Add Condition'),
      '#ajax' => [
        'callback' => [$this, 'addForm'],
        'event' => 'click',
      ],
    ];
    $form['items'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Plugin Id'),
        $this->t('Summary'),
        $this->t('Operations'),
      ],
      '#rows' => $this->renderRows(),
      '#empty' => $this->t('No conditions have been configured.'),
      '#prefix' => '<div id="entity-library-conditions">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (is_array($form_state->getValue('library_info'))) {
      $form_state->setValue('library_info', Yaml::encode($form_state->getValue('library_info')));
    }

    parent::submitForm($form, $form_state);

    $context = [
      '%name' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'), 'collection')->toString(),
    ];
    $this->logger('entity_library')->notice('Updated entity library %name.', $context);

    $this->messenger()->addMessage($this->t('The entity library has been updated.'));

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit']['#value'] = t('Save library definition');
    $actions['delete']['#value'] = t('Delete library definition');

    return $actions;
  }

  /**
   * Get contexts array.
   *
   * @return array
   *   The available contexts array of key value pairs suitable as '#options'
   *   for form elements.
   */
  protected function getContextOptionsForm() {
    static $options = NULL;

    if ($options === NULL) {
      $contexts = $this->contextRepository->getAvailableContexts();
      foreach ($this->conditionManager->getDefinitionsForContexts($contexts) as $plugin_id => $definition) {
        // Don't display the language condition until we have multiple
        // languages.
        if ($plugin_id == 'language' && !$this->languageManager->isMultilingual()) {
          continue;
        }

        $options[str_replace(':', "-", $plugin_id)] = (string) $definition['label'];
      }
    }

    return $options;
  }

  /**
   * Returns an array of supported operations for the conditions.
   *
   * @param string $route_name_base
   *   The name of the route.
   * @param array $route_parameters
   *   An associative array of parameter names and values.
   *
   * @return array
   *   The supported operations for the conditions.
   */
  protected function getOperations($route_name_base, array $route_parameters = []) {
    $operations = [];

    $operations['edit'] = [
      'title' => $this->t('Edit'),
      'url' => new Url($route_name_base, $route_parameters),
      'weight' => 10,
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'dialog',
        'data-dialog-renderer' => 'off_canvas',
        'data-dialog-options' => Json::encode([
          'width' => 400,
        ]),
      ],
    ];
    $operations['delete'] = [
      'title' => $this->t('Delete'),
      'url' => new Url($route_name_base . '.delete', $route_parameters),
      'weight' => 100,
      'attributes' => [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode([
          'width' => 700,
        ]),
      ],
    ];

    return $operations;
  }

  /**
   * Renders the conditions rows.
   *
   * @return array
   *   The table rows render array.
   */
  protected function renderRows() {
    $configured_conditions = [];
    foreach ($this->entity->getConditions() as $row => $condition) {
      $build = [
        '#type' => 'operations',
        '#links' => $this->getOperations('entity.entity_library.condition', [
          'entity_library' => $this->entity->id(),
          'id' => $row,
        ]),
      ];
      $configured_conditions[] = [
        'plugin_id' => $condition->getPluginId(),
        'summary' => $condition->summary(),
        'operations' => [
          'data' => $build,
        ],
      ];
    }

    return $configured_conditions;
  }

}
