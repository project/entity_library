<?php

namespace Drupal\entity_library\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Provides an interface defining a entity_library entity.
 */
interface EntityLibraryInterface extends ConfigEntityInterface, EntityWithPluginCollectionInterface {

  /**
   * Sets the label for the library.
   *
   * @param string $label
   *   The label for the library.
   *
   * @return $this
   */
  public function setLabel($label);

  /**
   * Returns the description for the library.
   *
   * @return string
   *   The description for the library.
   */
  public function getDescription();

  /**
   * Sets the description for the library.
   *
   * @param string $description
   *   The description for the library.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Returns the configuration for the library.
   *
   * @return array
   *   The configuration for the library.
   */
  public function getLibraryInfo();

  /**
   * Sets the library definition.
   *
   * @param string $library_info
   *   The definition for the library.
   *
   * @return $this
   */
  public function setLibraryInfo(string $library_info);

  /**
   * Returns the conditions this entity library used by.
   *
   * @return \Drupal\Core\Condition\ConditionInterface[]|\Drupal\Core\Condition\ConditionPluginCollection
   *   An array of configured condition plugins.
   */
  public function getConditions();

  /**
   * Adds a new condition to the library.
   *
   * @param array $configuration
   *   An array of configuration for the new entity library.
   *
   * @return string
   *   The condition ID.
   */
  public function addCondition(array $configuration);

  /**
   * Retrieves a specific condition.
   *
   * @param string $condition_id
   *   The condition ID.
   *
   * @return \Drupal\Core\Condition\ConditionInterface
   *   The condition object.
   */
  public function getCondition($condition_id);

  /**
   * Removes a specific condition.
   *
   * @param string $condition_id
   *   The condition ID.
   *
   * @return $this
   */
  public function removeCondition($condition_id);

  /**
   * Returns the logic used to compute the conditions, either 'and' or 'or'.
   *
   * @return string
   *   The string 'and', or the string 'or'.
   */
  public function getLogic();

  /**
   * Sets the logic used to compute the  conditions, either 'and' or 'or'.
   *
   * @param string $logic
   *   The access logic string.
   *
   * @return $this
   */
  public function setLogic($logic);

  /**
   * Clears static and persistent library definition caches.
   */
  public static function clearCachedLibraryDefinitions();

}
