<?php

namespace Drupal\entity_library\Entity;

use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines an entity library entity class.
 *
 * @ConfigEntityType(
 *   id = "entity_library",
 *   label = @Translation("Entity library"),
 *   label_singular = @Translation("entity library"),
 *   label_plural = @Translation("entity libraries"),
 *   label_count = @PluralTranslation(
 *     singular = "@count entity library",
 *     plural = "@count entity libraries"
 *   ),
 *   admin_permission = "administer entity libraries",
 *   handlers = {
 *     "list_builder" = "Drupal\entity_library\EntityLibraryListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_library\Form\EntityLibraryAddForm",
 *       "edit" = "Drupal\entity_library\Form\EntityLibraryEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *   },
 *   links = {
 *     "add-form" = "/admin/config/system/entity-library/add",
 *     "edit-form" = "/admin/config/system/entity-library/{entity_library}/edit",
 *     "delete-form" = "/admin/config/system/entity-library/{entity_library}/delete",
 *     "collection" = "/admin/config/system/entity-library"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "library_info",
 *     "conditions",
 *     "logic"
 *   }
 * )
 */
class EntityLibrary extends ConfigEntityBase implements EntityLibraryInterface {

  /**
   * The ID of the library.
   *
   * @var string
   */
  protected $id;

  /**
   * The label of the library.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the library.
   *
   * @var string
   */
  protected $description;

  /**
   * The definition of library.
   *
   * @var string
   */
  protected $library_info;

  /**
   * The configuration of conditions.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * Tracks the logic used to compute the conditions, either 'and' or 'or'.
   *
   * @var string
   */
  protected $logic = 'and';

  /**
   * The plugin collection that holds the conditions.
   *
   * @var \Drupal\Component\Plugin\LazyPluginCollection
   */
  protected $conditionCollection;

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->label = $label;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraryInfo() {
    return $this->library_info;
  }

  /**
   * {@inheritdoc}
   */
  public function setLibraryInfo(string $library_info) {
    $this->library_info = $library_info;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return [
      'conditions' => $this->getConditions(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    if (!$this->conditionCollection) {
      $this->conditionCollection = new ConditionPluginCollection(\Drupal::service('plugin.manager.condition'), $this->get('conditions'));
    }

    return $this->conditionCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function addCondition(array $configuration) {
    $configuration['uuid'] = $this->uuidGenerator()->generate();
    $this->getConditions()->addInstanceId($configuration['uuid'], $configuration);

    return $configuration['uuid'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCondition($condition_id) {
    return $this->getConditions()->get($condition_id);
  }

  /**
   * {@inheritdoc}
   */
  public function removeCondition($condition_id) {
    $this->getConditions()->removeInstanceId($condition_id);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogic() {
    return $this->logic;
  }

  /**
   * {@inheritdoc}
   */
  public function setLogic($logic) {
    $this->logic = $logic;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    self::clearCachedLibraryDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    self::clearCachedLibraryDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function clearCachedLibraryDefinitions() {
    \Drupal::service('library.discovery')->clearCachedDefinitions();
  }

}
